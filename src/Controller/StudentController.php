<?php

namespace App\Controller;

use App\Entity\Note;
use App\Entity\Student;
use App\Form\NoteType;
use App\Form\StudentType;
use App\Service\FormService;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\Controller\Annotations as Rest;

/**
 * Class StudentController
 * @package App\Controller
 * @Route("/api", name="api_")
 */
class StudentController extends AbstractFOSRestController
{

    /** @var EntityManagerInterface $em */
    private $em;

    /** @var FormService formService */
    private $formService;

    public function __construct(EntityManagerInterface $em, FormService $formService)
    {
        $this->em = $em;
        $this->formService = $formService;
    }

    /**
     * Post new student
     * @SWG\Post(
     *     path="/api/student/post",
     *     summary="Post new student",
     *     description="Post new student",
     *     @SWG\Parameter(
     *          name="student",
     *          in="body",
     *     @SWG\Schema(
     *          type="array",
     *          @Model(type=App\Entity\Student::class)
     *      )
     *     ),
     *     @SWG\Response(
     *          response=200,
     *          description="Success"
     *      )
     * )
     * @Rest\Post("/student/post")
     * @param Request $request
     *
     * @return Response
     */
    public function postStudent(Request $request)
    {
        $student = new Student();
        $data = json_decode($request->getContent(), true);
        $studentForm = $this->createForm(StudentType::class, $student);
        $studentForm->submit($data);
        if ($this->formService->checkForm($studentForm)) {
            $this->em->persist($student);
            $this->em->flush();

            return $this->handleView($this->view(['status' => 'OK'], Response::HTTP_CREATED));
        }

        return $this->handleView($this->view($studentForm->getErrors()));
    }

    /**
     * Post student note
     * @SWG\Post(
     *     path="/api/student/note/post",
     *     summary="Post new student note",
     *     description="Post new student note",
     *     @SWG\Parameter(
     *          name="note",
     *          in="body",
     *     @SWG\Schema(
     *          type="array",
     *          @Model(type=App\Entity\Note::class)
     *      )
     *     ),
     *     @SWG\Response(
     *          response=200,
     *          description="Success"
     *      )
     * )
     * @Rest\Post("/student/note/post")
     * @param Request $request
     *
     * @return Response
     */
    public function postNote(Request $request)
    {
        $note = new Note();
        $data = json_decode($request->getContent(), true);
        $noteForm = $this->createForm(NoteType::class, $note);
        $noteForm->submit($data);
        if ($this->formService->checkForm($noteForm)) {
            $this->em->persist($note);
            $this->em->flush();

            return $this->handleView($this->view(['status' => 'OK'], Response::HTTP_CREATED));
        }

        return $this->handleView($this->view($noteForm->getErrors()));
    }

    /**
     * GEt student notes
     * @SWG\Get(
     *     path="/api/student/{id}/notes",
     *     summary="Get student notes",
     *     description="Get student notes",
     *     @SWG\Parameter(
     *          name="id",
     *          in="path",
     *          type="string"
     *     ),
     *      @SWG\Response(
     *          response=200,
     *          description="Success"
     *      )
     * )
     * @Rest\Get("/student/{id}/notes")
     * @param Request $request
     *
     * @return Response
     */
    public function getStudentNotes(Request $request, int $id)
    {
        $results = [];
        $notes = $this->em->getRepository(Note::class)->findBy(['student' => $id]);
        /** @var Student $student */
        $student = $this->em->getRepository(Student::class)->findOneBy(['id' => $id]);
        $results['student'] = $student->getFName() . ' ' . $student->getLName();
        /** @var Note $note */
        foreach ($notes as $note) {
            $results['notes']['note'] = $note->getValue();
            $results['notes']['subject'] = $note->getSubject();
        }

        return $this->handleView($this->view(['result' => $results], Response::HTTP_OK));
    }

    /**
     * Update student
     * @SWG\Put(
     *     path="/api/student/update/{id}",
     *     summary="Update student informations",
     *     description="Update student informations",
     *     @SWG\Parameter(
     *          name="id",
     *          in="path",
     *          required=true,
     *          type="integer"
     *     ),
     *     @SWG\Parameter(
     *          name="student",
     *          in="body",
     *          @SWG\Schema(
     *             type="array",
     *             @Model(type=App\Entity\Student::class)
     *         )
     *     ),
     *      @SWG\Response(
     *          response=200,
     *          description="Success"
     *      )
     * )
     *
     * @Rest\Put("/student/update/{id}")
     * @param Request $request
     *
     * @return Response
     */
    public function updateStudent(Request $request, int $id)
    {
        $data = json_decode($request->getContent());
        $student = $this->em->getRepository(Student::class)->findOneBy(['id' => $id]);
        if ($student instanceof Student) {
            $student->setFName($data->fName ?? $student->getFName());
            $student->setLName($data->lName ?? $student->setLName());
            $student->setBDay(\DateTime::createFromFormat('Y/m/d', '1992/07/19') ?? $student->getBDay());

            $this->em->persist($student);
            $this->em->flush();

            return $this->handleView(new View(['message' => 'Student ' . $id . ' updated'], Response::HTTP_ACCEPTED));
        }

        return $this->handleView(new View(['message' => 'Student not found'], Response::HTTP_OK));
    }

    /**
     * Delete student
     * @SWG\Delete(
     *     path="/api/student/delete/{id}",
     *     summary="Delete student",
     *     description="Delete student",
     *     @SWG\Parameter(
     *          name="id",
     *          in="path",
     *          required=true,
     *          type="integer"
     *     ),
     *      @SWG\Response(
     *          response=200,
     *          description="Success"
     *      )
     * )
     *
     * @Rest\Delete("/student/delete/{id}")
     * @param Request $request
     *
     * @return Response
     */
    public function deleteStudent(Request $request, int $id)
    {
        $student = $this->em->getRepository(Student::class)->findOneBy(['id' => $id]);

        if ($student instanceof Student) {
            $this->em->remove($student);
            $this->em->flush();

            return $this->handleView(new View(['message' => 'Student ' . $id . ' removed'], Response::HTTP_ACCEPTED));
        }

        return $this->handleView(new View(['message' => 'Student not found'], Response::HTTP_OK));
    }

    /**
     *
     * Get Total Note
     * @SWG\Get(
     *     path="/api/student/total/{id}",
     *     summary="Get note total",
     *     description="Get note total",
     *     @SWG\Parameter(
     *          name="id",
     *          in="path",
     *          type="integer"
     *     ),
     *      @SWG\Response(
     *          response=200,
     *          description="Success"
     *      )
     * )
     * @Rest\Get("/student/total/{id}")
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function getStudentTotalNote(Request $request, int $id)
    {
        $student = $this->em->getRepository(Student::class)->findOneBy(['id' => $id]);
        if ($student instanceof Student) {
            $notes = $this->em->getRepository(Note::class)->findBy(['student' => $id]);
            if (count($notes) > 0) {
                $total = 0;
                /** @var Note $note */
                foreach ($notes as $note) {
                    $total += $note->getValue();
                }

                return $this->handleView(new View(['Total' => $total / count($notes)], Response::HTTP_OK));
            }
        }

        return $this->handleView(new View(['message' => 'Student not found'], Response::HTTP_OK));
    }

    /**
     *
     * Get Total Note Class
     * @SWG\Get(
     *     path="/api/student/total/class",
     *     summary="Get note total class",
     *     description="Get note total class",
     *      @SWG\Response(
     *          response=200,
     *          description="Success"
     *      )
     * )
     * @Rest\Get("/student/class/total")
     * @param Request $request
     * @return Response
     */
    public function getClassTotalNote(Request $request)
    {
        $students = $this->em->getRepository(Student::class)->findAll();
        if (count($students) > 0) {
            $classNotes = 0;
            /** @var Student $student */
            foreach ($students as $student) {
                $notes = $this->em->getRepository(Note::class)->findBy(['student' => $student->getId()]);
                if (count($notes) > 0) {
                    $total = 0;
                    /** @var Note $note */
                    foreach ($notes as $note) {
                        $total += $note->getValue();
                    }
                    $classNotes += $total;
                }
            }

            return $this->handleView(new View(['Total' => $classNotes / count($students)], Response::HTTP_OK));
        }

        return $this->handleView(new View(['message' => 'Student not found'], Response::HTTP_OK));
    }
}
