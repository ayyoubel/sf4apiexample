<?php


namespace App\Service;


use phpDocumentor\Reflection\Types\Boolean;
use Symfony\Component\Form\FormInterface;

class FormService
{
    public function checkForm(FormInterface $form): bool
    {
        return ($form->isSubmitted() && $form->isValid());
    }
}