<?php


namespace App\Tests\Controller;


use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class StudentControllerTest extends WebTestCase
{
    public function testgetStudentNotes()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/api/student/1/notes');
        $this->assertResponseStatusCodeSame(200, $client->getResponse()->getStatusCode());
    }

    public function testpostStudent()
    {
        $client = static::createClient();
        $client->request(
            'POST',
            '/api/student/post',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            '{"fName": "Test 3","lName": "Test 3","bDay": "1992/07/19"}'
        );
        $this->assertResponseStatusCodeSame(201, $client->getResponse()->getStatusCode());
    }
}